package com.sprysa;


import org.apache.logging.log4j.*;

public class LogTest {

  final static Logger logger = LogManager.getLogger(LogTest.class);

  public void logMessages() {
    logger.trace("This is a trace message");
    logger.debug("This is a debug message");
    logger.info("This is an info message");
    logger.warn("This is a warn message");
    logger.error("This is an error message");
    logger.fatal("This is a fatal message");
  }

}
